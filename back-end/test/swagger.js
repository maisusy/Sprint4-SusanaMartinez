const assert = require("chai").assert
const fetch = require("node-fetch")
const env = require("../src/config")

describe("#TEST DE SWAGGER",( ) => {

    it("API response 200",async() => {
        await fetch(env.SWAGGER)
        .then(response => {
            assert.equal(response.status,200)
        })
    }).timeout(10000);

})