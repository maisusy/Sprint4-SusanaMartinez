//**************************PAQUETES*************************************/
const express = require("express")
const Helmet = require("helmet")
const app = express()

//**************************CONFIGURACION CORDS*************************************/
const cors = require('cors')
app.use(cors())

//**************************CONFIGURACION EXPRESS*************************************/
app.use(express.urlencoded({extended:true}));
app.use(express.json())
app.use(Helmet(/* {
    directives: {
      defaultSrc: Helmet.contentSecurityPolicy.dangerouslyDisableDefaultSrc,
    },
  }) */))
app.use(express.static('./src/database/db.js'))

//**************************CONFIGURACION SWAGGER*************************************/
const YAML = require("yamljs")
const swaggerJcDoc = YAML.load("./SWAGGER.yaml");
const swaggerUi = require("swagger-ui-express");
app.use('/swagger',swaggerUi.serve,swaggerUi.setup(swaggerJcDoc));

//**************************CONFIGURACION PASSPORT*************************************/
const initServices = require('./src/services');
const passport = require('passport')
const prepareRoutes = require('./src/router/auth');
initServices(app);  
app.use(prepareRoutes());
app.use(passport.initialize());

//**************************RUTA PRINCIPAL*************************************/
const ApiRuta = require("./src/router/api")
app.use("/resto",ApiRuta)

//**************************CONFIGURACION FRONT-END*************************************/
/* app.use(express.static('./public/dist')); */
app.get('/bienvenido',(req,res)=>{
  res.send({
    msg:"HOLAAAA"
  })
} )

//***************************ESCUCHANDO EL SERVIDOR*******************************************/
const env = require("./src/config/index")
app.listen(env.PORT,
    ()=>{console.log(`Escuchando servidor en: ${env.DB_HOST}:${env.PORT} \nEscuchando swagger en :${env.SWAGGER}`) });
