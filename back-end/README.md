PROYECTO DELLILAH RESTO
Aministracion de datos de un restaurante

DOMINIO : https://www.acamicadelilahresto.tk

REQUISITOS 📋
SE NECESITAN LOS SIGUIENTES PAQUETES CON LAS SIGUIENTES VERSIONES:
        "axios": "^0.25.0",
        "chai": "^4.3.4",
        "chai-http": "^4.3.0",
        "cors": "^2.8.5",
        "dotenv": "^10.0.0",
        "helmet": "^4.6.0",
        "jsonwebtoken": "^8.5.1",
        "mocha": "^9.1.3",
        "moment": "^2.29.1",
        "mongoose": "^6.0.4",
        "node-fetch": "^2.6.1",
        "passport": "^0.5.2",
        "passport-facebook": "^3.0.0",
        "passport-google-oauth20": "^2.0.0",
        "redis": "^4.0.0",
        "swagger-ui-express": "^4.1.6",
        "yamljs": "^0.3.0",
        "express": "^4.17.1",
        "nodemon": "^2.0.12"

INSTALACION🔧
SERVER
Lo primero es crear una carpeta en el escrito para guardar el programa.Hecho esto debe dirijirse a la terminal de su computadora y con el comando "cd /'ruta_de_la_carpeta'" posicionarse en la carpeta anteriormente creada.
Ya en la carpeta debe poner el comando "git init" y posteriormente el "git clone" junto al repositorio(https://gitlab.com/maisusy/Sprint4-SusanaMartinez.git).
Luego de que se descargue simplemente abre la carpeta con el visual studio.
Se debe dirigir a la terminal del Visual Studio y en la misma instalar los paquetes con los siguientes comandos:
    -npm i 
Luego de la instalcion de los paquetes se inicia el programa con el comando "npm run dev"

DOCKER
Primero hacer la imagen con los siguientes comando "docker build --tag back-end ."(dentro de la carpeta del back-end) y luego para correr la imagen "docker run --name back-end -p 8080:8080 back-end"

BASE DE DATOS
Para conectarse a la base datos usara mongocompass el cual debera instalar.Para esto le dejamos la pagina la cual le proporcionara las instrucciones necesarias para su uso.
link:https://docs.mongodb.com/compass/current/install/
Luego de su instalacion ingresara a mongoDBCompass e inserta los siguiente "mongodb+srv://DelilahResto:AcamicaPruebaUsuario431298@sprint-acamica.pyntb.mongodb.net/test?authSource=admin&replicaSet=atlas-lhy2vw-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true" en donde dige "Click edit to modify your connection string (SRV or Standard )"despues solamente click en "Connect" y listo ya tiene la base de datos conectada. 

TEST
Para el testeo debe ingresar en la consola y usa el comando  "npm test"

USO DE POSTMAN
A continuacion se le dejara un video para saber la utilizacion del token en postman.
link:https://youtu.be/7c6w_Ezw_UA
Y para mas informacion debajo dejare una pequña descripcion de lo que hace cada ruta(https://www.maisusy-acamica.tk/resto):
INICIO
/inicio/login Nos permite iniciar sesion
/inicio/alta Nos permite crear un nuevo usuario
/inicio/CerrarSesion Nos permite cerrar la sesion
PEDIDO
/pedidos/modificar/estado/{num} Nos permite modificar el estado de un pedido
/pedidos/altaPedido Nos permite registrar un pedido
/pedidos/historial Nos mostrara un listado de los pedidos 
PRODUCTO
/productos/ Nos mostrara un listado de los producto 
/productos/alta Nos permite registrar un nuevo producto
/productos/modificar/{nombre} Nos permite modifcar un producto
/productos/baja/{nombre} Nos permite eliminar un producto
USUARIO
/usuarios/ Nos muestra un listado de los usuarios
/usuarios/bajaCuenta  Nos permite eliminar la cuenta con la que estemos logueados en el momento
/usuarios/actualizarCuenta Nos permite actualizar la cuenta con la que estemos logueados en el momento
/usuarios/SuspenderCuenta/{user} Nos permite suspender a un usuario 
/usuarios/CancelarSuspencion/{user} Nos permite anular la suspencion de un usuario
MEDIOS DE PAGO
/medio_de_pago/ Nos muestra un listado de medios de pago
/medio_de_pago/alta Nos permite crear un nuevo medio de pago
/medio_de_pago/modificar/{nombre} Nos permite modificar un medio de pago
/medio_de_pago/baja/{nombre} Nos permite eliminar un medio de pago

USO DE SWAGGER
En el siguiente link aparecera el uso del token en swagger(https://www.maisusy-acamica.tk/swagger)
link:https://youtu.be/kwV8_FCne9w

Autor ✒️
-nombre:Susana Martinez
    -correo electronico: maisusy15@outlook.com
    -repositorio:https://gitlab.com/maisusy/sprint_1-susana_martinez.git

