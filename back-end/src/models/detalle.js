const {Schema} = require("../database/db")
const Producto = require("./producto")

const Detalle = new Schema({
    num_pedido : Number,
    Producto : Producto.producto,
    Cantidad : Number
})

exports.Detalle = Detalle