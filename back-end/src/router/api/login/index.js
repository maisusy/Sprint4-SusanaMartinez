const router = require("express").Router()
const UsuarioMiddleware = require("../../../middleware/usuarios")
const controller = require("../../../controller/login")

router.post(
    "/login",
    UsuarioMiddleware.deslogeo,
    UsuarioMiddleware.DatosUsuario_Login,
    UsuarioMiddleware.ExistenciaUsuario,
    UsuarioMiddleware.suspencion,
    controller.login
)


router.post(
    "/alta",
    UsuarioMiddleware.DatosUsuario_Login,
    UsuarioMiddleware.ExistenciaUsuario,
    UsuarioMiddleware.Email,
    controller.alta
) 



 router.put(
    "/CerrarSesion",
    controller.cerrarSesion
 )

module.exports = router;