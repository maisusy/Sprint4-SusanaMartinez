const router = require("express").Router()
const controller = require("../../../controller/producto")
const UsuarioMiddleware = require("../../../middleware/usuarios")
const ProductoMiddleware = require("../../../middleware/productos")



router.get("/",controller.listado)

router.use(UsuarioMiddleware.es_admin)

router.post("/alta",
    ProductoMiddleware.DatosProducto,
    ProductoMiddleware.ExistenciaProducto,
    ProductoMiddleware.validarPrecio,
    controller.alta
)

router.delete("/baja/:nombre",
    ProductoMiddleware.DatosProducto,
    ProductoMiddleware.ExistenciaProducto,
    controller.eliminar
)

 router.put("/modificar/:nombre",
    ProductoMiddleware.DatosProducto,
    ProductoMiddleware.ExistenciaProducto,
    ProductoMiddleware.validarPrecio,
    controller.actualizar
 )


module.exports = router;