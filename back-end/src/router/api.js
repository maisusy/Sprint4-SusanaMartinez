const router = require("express").Router();
const ProductoRuta = require("./api/producto")
const PedidoRuta = require("./api/pedidos")
const UsuarioRuta = require("./api/usuario")
const MedioDePagoRuta = require("./api/medios_de_pago")
const LoginRuta = require("./api/login")
const UsuarioMiddleware = require("../middleware/usuarios")

router.use(
    "/inicio",
    LoginRuta
)


router.use(
    "/productos",   
    UsuarioMiddleware.AunteticacionToken,
    UsuarioMiddleware.esta_logueado,
    ProductoRuta
)

router.use(
    "/pedidos",
    UsuarioMiddleware.AunteticacionToken,
    UsuarioMiddleware.esta_logueado,
    PedidoRuta
)

router.use(
    "/usuarios",
    UsuarioMiddleware.AunteticacionToken,
    UsuarioMiddleware.esta_logueado,
    UsuarioRuta
)

router.use(
    "/medio_de_pago",
    UsuarioMiddleware.AunteticacionToken,
    UsuarioMiddleware.esta_logueado,
    MedioDePagoRuta
)


module.exports = router;
