const express = require('express');
const router = express.Router();
const google = require('./google');
const facebook = require('./facebook'); /* 
const linkedin = require('./linkedin');  */

function prepareRoutes() {
  router.use(facebook());
  router.use(google());/* 
  router.use(linkedin()); */
  return router
}

module.exports = prepareRoutes;