const express = require('express');
const { linkedinAuthenticate, linkedinTokenAquisition } = require('../../../middleware/linkedin');

function prepareRouter() {
  const router = express.Router();

  router.get('/linkedin/auth', linkedinAuthenticate);

  router.get('/linkedin/auth/callback', linkedinTokenAquisition, (req, res) => {
    console.log(req.user)
    res.json(req.user)
  });


  return router;
}

module.exports = prepareRouter;
