const express = require('express');
const passport = require('passport');
const Usuarios = require("../../../controller/usuario").Usuarios
const jwt = require("jsonwebtoken")
const firma = "token"

function prepareRouter() {
  const strategy_name = 'google';
  const router = express.Router();
  
  router.get('/google/auth', passport.authenticate(strategy_name, {
    scope: ['profile', 'email'],
    session: false,
  }));

router.get("/login",(req,res)=>{
  res.send({
    msq:"usted esta en el login"
  })
})

router.get("/menu",(req,res)=>{
  res.send({
    msq:"usted esta en el menu"
  })
})

  router.get('/google/auth/callback', passport.authenticate(strategy_name, 
   {
      failureRedirect: '/login',
      session: false,
    }),
    function (req, res) {
      Usuarios.findOne({
        correo: req.user._json.email,
      })
      .then(result => {
          result.logueado = true;
          result.save()
          const token = jwt.sign({
            usuario: result.usuario,
            nom_ape: result.nom_ape,
            correo: result.correo,
            telefono: result.telefono,
            direccion: result.direccion,
            contrasenia: result.contrasenia,
            admin: result.admin,
            logueado: true,
            suspendido: result.suspendido
          }, firma, { expiresIn: "1h" })
      res.redirect(`/menu?token=${token}`);
    });
    }
  );
  return router;
}

module.exports = prepareRouter;
