const passport = require('passport');
const passportFacebookStrategy = require('passport-facebook');
const env = require('../../config')
const Usuarios = require("../../controller/usuario").Usuarios

function prepareStrategy() {
  const FacebookStrategy = passportFacebookStrategy.Strategy;
  
  const strategy_name = 'facebook';
  
  passport.use(strategy_name, new FacebookStrategy({
      clientID: env.FACEBOOK_CLIENT_ID,
      clientSecret: env.FACEBOOK_CLIENT_SECRET,
      callbackURL: env.FACEBOOK_CALLBACK,
    },
    function(accessToken, refreshToken, profile, done) {
      Usuarios.findOne({
        correo: profile._json.email,
      })
      .then( result => {
        if(result){
          return done(null,profile)
        }else{
          err = new Error("no se encontro el usuario con el email :"+profile._json.email)
          return done(err,profile)
        }
        
      })
      .catch( err => {
        return done(err,profile)
      })
    }
  ));
}

module.exports = prepareStrategy;