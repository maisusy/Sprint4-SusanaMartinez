const passport = require('passport');
const passportGoogleStrategy = require('passport-google-oauth20');
const env = require('../../config')
const Usuarios = require("../../controller/usuario").Usuarios

function prepareStrategy() {
  const GoogleStrategy = passportGoogleStrategy.Strategy;

  const strategy_name = 'google';

  passport.use(strategy_name, new GoogleStrategy({
    clientID: env.GOOGLE_CLIENT_ID,
    clientSecret: env.GOOGLE_CLIENT_SECRET,
    callbackURL: env.GOOGLE_CALLBACK,
  },
    function (accessToken, refreshToken, profile, done) {
      Usuarios.findOne({
        correo: profile._json.email,
      })
      .then( result => {
        if(result){
          return done(null,profile)
        }else{
          err = new Error("no se encontro el usuario con el email :"+profile._json.email)
          return done(err,profile)
        }
        
      })
      .catch( err => {
        return done(err,profile)
      })
      
    }
  ));
}

module.exports = prepareStrategy;