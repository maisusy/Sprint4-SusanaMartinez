const Schema = require("../models/usuario").usuario
const con = require("../database/db")
const Usuarios = con.model("Usuarios",Schema)
const jwt = require("jsonwebtoken")
const firma = "token"

exports.Usuarios = Usuarios

exports.lista = (async(req,res)=>{

    await Usuarios.find()
    .then( result =>{
        res.status(200).json(
            {
                msg:"listado de usuarios",
                datos:result
            }
        )
    })
    .catch( err =>{
        res.status(404).json(
            {
                msg:"HA OCURRIDO UN ERROR AL LISTAR USUARIOS",
                error:err
            }
        )
    })
})

exports.baja = (async(req,res)=>{

    await Usuarios.deleteOne({usuario:req.body.token.usuario})
    .then( (result)=>{
        res.status(200).json(
            {
                msg:"usuario eliminado",
                datos:result
            }
        )
    })
    .catch(err => {
        res.status(404).json(
            {
                msg:"HA OCURRIDO UN ERROR AL ELIMINAR USUARIO",
                error:err
            }
        )
    })

})

exports.actualizar = (async (req,res) =>{
const {usuario,nom_ape,correo,telefono,direccion,contrasenia} = req.body;
    try{
        await Usuarios.findOne({usuario:req.body.token.usuario})
        .then( result => { 
            result.usuario = usuario
            result.nom_ape = nom_ape
            result.correo = correo
            result.telefono = telefono
            result.contrasenia = contrasenia
            result.direccion = direccion
            result.save() 
            const token = jwt.sign({
                usuario : result.usuario,
                nom_ape : result.nom_ape,
                correo : result.correo,
                telefono : result.telefono,
                direccion : result.direccion,
                contraseniaa : result.contrasenia,
                admin : result.admin,
                logueado : true,
                suspendido : result.suspendido
            },firma,{expiresIn : "1h"})
            res.status(200).json(
                    {
                        msg:"usuario modificado",
                        token:token
                    }
                )
        });
    }catch (err){
        res.status(404).json(
            {
                msg:"HA OCURRIDO UN ERROR AL ACTUALIZAR EL USUARIO",
                error:err
            }
        )
    }

})

exports.suspender = async (req,res) => {
    try{
        await Usuarios.findOne({usuario : req.params.user})
        .then(result => {
            result.suspendido = true
            result.save()
            res.status(200).json(
                {
                    msg:"Usuario suspendido con exito",
                    datos:result
                })
        })
    }catch(err){
        res.status(404).json({
            msg:"HA OCURRIDO UN ERROR AL SUSPENDER A UN USUARIO",
            error: err
        })
    }
    
}

exports.calcelarsuspencion = async (req,res) => {
    try{
        await Usuarios.findOne({usuario : req.params.user})
        .then(result => {
            result.suspendido = false
            result.save()
            res.status(200).json(
                {
                    msg:"anulacion de suspencion de usuario exitosa",
                    datos:result
                })
        })
    }catch(err){
        res.status(404).json({
            msg:"HA OCURRIDO UN ERROR AL SUSPENDER A UN USUARIO",
            error: err
        })
    }
    
}

exports.AgregarDireccion = async(req,res) => {
    try{
        const usuarios = await Usuarios.find()
        
        usuarios.forEach( item => {
            if(item.usuario == req.body.token.usuario){
                nuevadireccion = item.direccion
            }
        })

        console.log(nuevadireccion)
         
        nuevadireccion.push(req.body.direccion)

        console.log(nuevadireccion)

        await Usuarios.updateOne({usuario: req.body.token.usuario},{direccion : nuevadireccion})

        res.status(200).json({
            msg:"SE HA REGISTRADO EXITOSAMENTE LA DIRECCION",
            error: er
        })

    }catch(er){
        res.status(404).json({
            msg:"HA OCURRIDO UN ERROR AL INTENTAR AGREGAR UNA NUEVA DIRECCION",
            error: er
        })
    }


}