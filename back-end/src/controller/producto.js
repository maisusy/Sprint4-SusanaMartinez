const Schema = require("../models/producto").producto
const con = require("../database/db")
const Productos = con.model("Productos",Schema)

//**************************REDIS*************************************/
const redis = require("redis");
const client = redis.createClient({
    url:"redis://redis-001.imaljd.0001.sae1.cache.amazonaws.com:6379"
}); 

client.connect()
.then(() => {console.log("conectado a redis")})


client.on("error",function (err){
    console.log(err)
})


exports.Productos = Productos

exports.listado = async(req,res)=>{

   try{

        client.get("PRODUCTOS",(err,result)=>{
            if(err){
                return res.status(400).json({
                    msg:"ocurrio un error",
                    error:error
                })
            }
            return res.status(200).json({
                    msg:"Listado de productos",
                    datos: result
                })
        });
        
        const prod = await Productos.find()
        client.set("PRODUCTOS",JSON.stringify(prod),'EX',10*60*60,(err)=>{
            if(err){
                return res.status(400).json({
                    msg:"ocurrio un error",
                    error:err
                })
            }
        })
        return res.status(200).json({
            msg:"Listado de productos",
            datos: prod
        })

    }catch(err){
        return res.status(404).json({
            msg:"HA OCURRIDO UN ERROR AL LISTAR LOS PRODUCTOS",
            error:err
        })
    }
 
        
};

exports.alta = ( async (req,res) => {

    const datos = new Productos ({
        nombre:req.body.nombre,
        precio:parseInt(req.body.precio)
    });

    await datos.save()

    .then( result => {
        res.status(200).json({
            msg:"Creacion de producto",
            datos: result
        })
    })
    .catch(err => {
        res.status(404).json({
            msg:"HA OCURRIDO UN ERROR AL CREAR EL PRODUCTO",
            error:err
        })
    })

});

exports.actualizar = ( async (req,res) => {

    try{
        await Productos.findOne({nombre : req.params.nombre})
        .then( result => {
            result.nombre = req.body.nombre
            result.precio = req.body.precio
            result.save()
            res.status(200).json({
                msg:"Actualizacion del producto",
                datos: result
            })
        })
    }catch(err){
        res.status(404).json({
            msg:"HA OCURRIDO UN ERROR AL ACTUALIZAR EL PRODUCTO",
            error:err
        })
    }
    
})

exports.eliminar = ( async (req,res) => {

    await Productos.deleteOne({nombre : req.params.nombre})
    .then( result => {
        res.status(200).json({
            msg:"Elimacion del producto",
            datos: result
        })
    })
    .catch(err => {

        res.status(404).json({
            msg:"HA OCURRIDO UN ERROR AL ELIMINAR EL PRODUCTO",
            error:err
        })

    })

})
