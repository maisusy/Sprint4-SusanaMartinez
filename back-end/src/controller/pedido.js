const moment = require("moment")
const Schema = require("../models/pedido").Pedido
const con = require("../database/db")
const Pedidos = con.model("Pedidos",Schema)

const estado = require("../middleware/estados").Estados
const MediosDePago = require("../controller/medios_de_pago").MedioDePago
const SchemaDetalle = require("../models/detalle").Detalle
const Detalle = con.model("Detalles",SchemaDetalle)

exports.Pedidos = Pedidos

exports.alta = ( async(req,res) => { 

    const{pedidos_,forma_de_pago}=req.body;
    let desc = String()
    let total = Number()
    let token = req.body.token

    async function crearPedido() {
        
        const Session = await con.startSession()

        await Session.startTransaction()

        try{

            let est = await estado.findOne({nom : "pendiente"})

            let mdp = await MediosDePago.findOne({nom : forma_de_pago})

            let ped = await Pedidos.find()

            let hora = moment().format("LT");


            const pedido = new Pedidos({
                num :  ped[ped.length-1].num + 1,
                hora : hora,
                direccion : token.direccion,
                estado : {
                    _id : est._id,
                    nom : est.nom
                },
                forma_de_pago:{
                    _id : mdp._id,
                    nom : mdp.nom
                },
                usuario:{
                    usuario : token.usuario,
                    nom_ape : token.nom_ape,
                    correo : token.correo,
                    telefono : token.telefono,
                    contrasenia : token.contrasenia
                },
                direccion : req.body.direccion
            })

            pedidos_.forEach(async(item) =>{
                desc = desc + `x${item.cantidad}${item.producto.nombre}, `;
                total = total + parseInt(item.cantidad * item.producto.precio);
                let det = new Detalle({
                    num_pedido : ped[ped.length-1].num + 1,
                    Producto : item.producto,
                    Cantidad : item.cantidad
                })
                pedido.detalle_productos.push(det)
                det.save()
            })

            pedido.descripcion = desc
            pedido.total = total

            pedido.save()
            Session.commitTransaction()


            res.status(200).json({
                msg : "Pedido registrado con exito",
                datos : pedido
            })

        }catch(err){
            Session.abortTransaction()
            res.status(404).json({
                msg : "HA OCURRIO UN ERROR AL CREAR EL PEDIDO",
                error : err
            })
        }

        Session.endSession()
    }

    crearPedido()
    
})

exports.historial =  async (req,res) => {

    const token = req.body.token

    try{
        if(token.admin == true){
            await Pedidos.find()
            .then((result) => {
                res.status(200).json({
                    msg : "Listado de pedidos",
                    datos : result
                })
            })
    
        }else{
            await  Pedidos.find({ "usuario.usuario" : token.usuario})
            .then((result) => {
                if(result.length >= 1){
                    res.status(200).json({
                        msg : `Listado de pedidos del usuario : ${token.usuario}`,
                        datos : result
                    })
                }else{
                    res.status(200).json({
                        msg : `No se encontraron pedidos asociados a esta cuenta`
                    })
                }
            })
        }
    }catch(err){
            res.status(404).json({
                msg : "HA OCURRIDO UN ERROR AL LISTAR LOS PEDIDOS",
                error : err
            })
        }

} 

exports.ActualizarEstado = async(req,res) => {

    try{
        await Pedidos.findOne({num : req.params.num})
        .then(async(result) => {
            const est = await estado.findOne({nom : req.body.estado})
            result.estado = {
                _id: est._id,
                nom : est.nom
            }
            result.save()
            res.status(200).json({
                msg : "Estado actualizado",
                datos: result
            })
        })

    }catch(err){
        res.status(500).json({
            msg : "HA OCURRIDO UN ERROR AL ACTUALIZAR EL ESTADO",
            error : err
        })
    }

}