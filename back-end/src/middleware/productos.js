const Producto = require("../controller/producto").Productos

function validarPrecio(req,res,next){
    const{precio}=req.body;
    if(precio<=0){
        res.status(404).json({msg : "EL PRECIO DEBE SER MAYOR O IGUAL A UNO(1),REVISE NUEVAMENTE LOS DATOS"})
    }else{
        next();
    }
}

async function ExistenciaProducto(req,res,next){

    switch(req.url){
        case "/altaPedido":
            const{pedidos_}=req.body;
            pedidos_.forEach( async(item) => {
                await Producto.findOne({nombre : item.producto.nombre , precio : item.producto.precio})
                .then(result => {
                    if(result == null || result == undefined){
                        res.status(404).json({msg : `EL PRODUCTO DE NOMBRE '${item.producto.nombre}' y precio '${item.producto.precio}' NO  EXISTE`})
                    }
                })
                .catch(err => {
                    res.status(404).json({
                        msg : "HA OCURRIDO UN ERROR AL VALIDAR EXISTENCIA DEL PRODUCTO",
                        error : err
                    })
                })
            })
            next();
        break;
        case `/alta`:
            await Producto.findOne({nombre : req.body.nombre})
            .then(result => {
                if(result != undefined){
                    res.status(404).json({msg : `EL PRODUCTO DE NOMBRE:${req.body.nombre} YA  EXISTE`})
                }else{
                    next();
                }
            })
            .catch(err => {
                res.status(404).json({
                    msg : "HA OCURRIDO UN ERROR AL VALIDAR EXISTENCIA DEL PRODUCTO",
                    error : err
                })
            })
        break;
        case `/modificar/${req.params.nombre.replaceAll(" ","%20")}`:
            try{
                await Producto.findOne({nombre : req.params.nombre})
                .then(async(result) => {
                    if(result == undefined){
                        res.status(404).json({msg : `El producto buscado no existe`})
                    }else{
                            await Producto.findOne({nombre : req.body.nombre})
                            .then(async(result) => {
                                if(result != undefined){
                                    res.status(404).json({msg : `El nombre de este producto ya pertece a otro.Intente con otro nombre`})
                                }else{
                                    next();
                                }
                            })
                        }
                })
            }catch(err){
                res.status(404).json({
                    msg : "HA OCURRIDO UN ERROR AL VALIDAR LA EXISTENCIA DEL PRODUCTO",
                    error : err
                })
            }
        break;
        case `/baja/${req.params.nombre.replaceAll(" ","%20")}`:
            await Producto.findOne({nombre : req.params.nombre})
            .then(result => {
                if(result == undefined){
                    res.status(404).json({msg : `El producto que esta buscado no existe`})
                }else{
                    next();
                }
            })
            .catch(err => {
                res.status(404).json({
                    msg : "HA OCURRIDO UN ERROR AL VALIDAR LA EXISTENCIA DEL PRODCUTO",
                    error : err
                })
            })
        break;
    }
}

function DatosProducto(req,res,next){
    switch(req.url){ 
        case "/alta" : 
        if( !req.body.nombre || !req.body.precio){
            res.status(404).json({msg : "SE REQUIEREN TODOS LOS CAMPOS"})
        }else{
            if(req.body.nombre!=="" || req.body.precio!==""){
                next()
            }else{
                res.status(404).json({msg : "LOS CAMPOS NO DEBES ESTAR VACIOS"})
            }
        }
        break;
        case `/modificar/${req.params.nombre.replaceAll(" ","%20")}`:
            if( !req.params.nombre || !req.body.nombre || !req.body.precio){
                res.status(404).json({msg : "SE REQUIEREN TODOS LOS CAMPOS"})
            }else{
                if(req.params.nombre !=="" || req.body.nombre !== ""|| req.body.precio !=""){
                    next()
                }else{
                    res.status(404).json({msg : "LOS CAMPOS NO DEBES ESTAR VACIOS"})
                }
            }
            break;
        case `/baja/${req.params.nombre.replaceAll(" ","%20")}`:
            if( !req.params.nombre ){
                res.status(404).json({msg : "SE REQUIEREN TODOS LOS CAMPOS"})
            }else{
                if(req.params.nombre !==""){
                    next()
                }else{
                    res.status(404).json({msg : "LOS CAMPOS NO DEBES ESTAR VACIOS"})
                }
            }
        break;
    }

}

module.exports = {
    validarPrecio,
    ExistenciaProducto ,
    DatosProducto 
}