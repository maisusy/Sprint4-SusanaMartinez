
const Schema = require("../models/estado").estado
const con = require("../database/db")
const Estados = con.model("Estados",Schema)

async function ExistenciaEstado(req,res,next){
    try{

        const est = await Estados.find()

        await Estados.findOne({nom :  req.body.estado})
        .then(result => {
            if(result!=undefined){
                next()
            }else{
                res.status(404).json({
                    msg : `No se encontro el estado de nombre'${req.body.estado}' \nPruebe con alguno de estos:`,
                    datos:est
                })
            }
        })

    }catch(err){
        res.status(404).json({
            msg : "HA OCURRIDO UN ERROR AL VALIDAR LA EXISTENCIA DEL ESTADO",
            error : err
        })
    }

}

module.exports = {
    ExistenciaEstado,
    Estados
}
