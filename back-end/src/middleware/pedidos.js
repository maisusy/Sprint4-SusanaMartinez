const Pedido = require("../controller/pedido").Pedidos
const Usuarios = require("../../src/controller/usuario").Usuarios

function DatosPedidos(req,res,next){
    switch (req.url) {
        case `/modificar/estado/${req.params.num}`:
            if(!req.params.num || !req.body.estado  ){
                res.status(404).json({ msg : "SE REQUIEREN TODOS LOS CAMPOS"})
            }else{
                if(req.params.num!=="" || req.body.estado!==""){
                    next()
                }else{
                    res.status(404).json({ msg : "LOS CAMPOS NO DEBEN ESTAR VACIOS"})
                }
            }
            break;
        case "/altaPedido":
            const{pedidos_,forma_de_pago} = req.body;
            if(!pedidos_ || !forma_de_pago  ){
                res.status(404).json({ msg : "SE REQUIEREN TODOS LOS CAMPOS"})
            }else{
                if( pedidos_!=="" || forma_de_pago!==""){
                    next()
                }else{
                    res.status(404).json({ msg : "LOS CAMPOS NO DEBEN ESTAR VACIOS"})
                }
            }
            break;
    }


}

async function ExistenciaPedido(req,res,next){

        await Pedido.findOne({num : req.params.num})
        .then( result => {
            if(result != undefined){
                next();
            }else{
                res.status(404).json({
                    msg : `PEDIDO N° ${req.params.num} NO ENCONTRADO`
                })
            }
        })
        .catch( error => {
            res.status(404).json({
                msg : `HA OCURRIDO UN ERROR AL VERIFICAR LA EXISTENCIA DEL PEDIDO`,
                error : error
            })
        })

}       

function validarCantidad(req,res,next){
    const{pedidos_}=req.body;
    let ban = false;
    
    pedidos_.forEach(item => {
        if(item.cantidad <= 0 ){
            ban = true;
        }
    });
    if(ban===true){
        res.status(404).json({msg : "LAS CANTIDADES DEBEN SER MAYORES A CERO,REVISE NUEVAMENTE LOS DATOS"})
    }else{
        next();
    }
}

async function validarDireccion(req,res,next){
    try{
            const usuarios = await Usuarios.find()
        
            usuarios.forEach( item => {
                if(item.usuario == req.body.token.usuario){
                    direccion = item.direccion
                    console.log( direccion)
                }
            })
    
            ban = false

            direccion.forEach( item => {
                if(item == req.body.direccion)ban=true
            })


            console.log( ban)
            if(ban == false){
                res.status(404).json({
                    msg:"No existe la direccion"
                })
            }else{
                next()
            }
    }catch(err){
        res.status(404).json({
            msg:"HA OCURRIDO UN ERROR EN LA VALIDACION DEL PEDIDO",
            error: err
        })
    }
            
}

module.exports = {
    DatosPedidos,
    ExistenciaPedido,
    validarCantidad,
    validarDireccion
}
