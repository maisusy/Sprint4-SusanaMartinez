require('dotenv').config()
module.exports = {

    DB_PASS : process.env.DB_PASSWORD,
    DB_DATABASE : process.env.DB_NAME,
    DB_USER: process.env.DB_USER,
    DB_HOST : process.env.DB_HOST,
    SWAGGER : process.env.SWAGGER, 
    PORT: process.env.PORT,
    GOOGLE_CLIENT_ID : process.env.GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET : process.env.GOOGLE_CLIENT_SECRET,
    GOOGLE_CALLBACK : process.env.GOOGLE_CALLBACK,
    FACEBOOK_CLIENT_ID : process.env.FACEBOOK_CLIENT_ID,
    FACEBOOK_CLIENT_SECRET : process.env.FACEBOOK_CLIENT_SECRET,
    FACEBOOK_CALLBACK : process.env.FACEBOOK_CALLBACK
}
