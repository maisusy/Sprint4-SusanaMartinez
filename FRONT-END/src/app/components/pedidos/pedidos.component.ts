import { Component, OnInit } from '@angular/core';
import { PedidosService } from './pedidos.service';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.css']
})
export class PedidosComponent implements OnInit {

  public pedidos : any = [];

  constructor(
    public PedidosService : PedidosService,
  ) { }

  ngOnInit(): void {
    this.ListadoMediosdepago()
  }


  ListadoMediosdepago(){
    this.PedidosService.ObtenerPedidos().subscribe(
      res => {
        console.log(res.datos)
        this.pedidos = res.datos
      },
      err => {
        console.log(err)
      }
    )
  }

}
