import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PedidosComponent } from './pedidos.component';
import { TableModule } from 'primeng/table';



@NgModule({
  declarations: [
    PedidosComponent
  ],
  exports: [
    PedidosComponent
  ],
  imports: [
    CommonModule,
    TableModule,
  ]
})
export class PedidosModule { }
