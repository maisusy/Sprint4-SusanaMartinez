import { Component, OnInit } from '@angular/core';
import { UsuarioService } from './usuario.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  public Usuarios : any = [];

  constructor(
    public UsuariosService : UsuarioService,
  ) { }

  ngOnInit(): void {
    this.ListadoMediosdepago()
  }


  ListadoMediosdepago(){
    this.UsuariosService.ObtenerUsuarios().subscribe(
      res => {
        console.log(res.datos)
        this.Usuarios = res.datos
      },
      err => {
        console.log(err)
      }
    )
  }

}
