import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuarioComponent } from './usuario.component';
import { TableModule } from 'primeng/table';



@NgModule({
  declarations: [
    UsuarioComponent
  ],
  exports : [
    UsuarioComponent
  ],
  imports: [
    CommonModule,
    TableModule
  ]
})
export class UsuarioModule { }
