import { Component, OnInit } from '@angular/core';
import { ProductosService } from './productos.service'

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  public productos : any = [];

  constructor(
    public ProductosService : ProductosService,
  ) { }

  ngOnInit(): void {
    this.ListadoProdutos()
  }


  ListadoProdutos(){
    this.ProductosService.ObtenerProductos().subscribe(
      res => {
        console.log(res.datos)
        this.productos = res.datos
      },
      err => {
        console.log(err)
      }
    )
  }

}
