import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RespuestaGeneral } from 'src/app/models/general';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  public URL : string = environment.apiURL

  constructor(
    private http: HttpClient
  ) { }

  ObtenerProductos():Observable <RespuestaGeneral>{
    return this.http.get<RespuestaGeneral>(`${this.URL}/resto/productos/`)
  }
}
