import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TableModule} from 'primeng/table';
import { ProductosComponent } from './productos.component';


@NgModule({
  declarations: [
    ProductosComponent,
  ],
  exports: [
    ProductosComponent
  ],
  imports: [
    CommonModule,
    TableModule,
  ]
})
export class ProductosModule { }
