import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RespuestaGeneral } from 'src/app/models/general';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MediosPagoService {

  public URL : string = environment.apiURL

  constructor(
  private http: HttpClient
  ) { }

  ObtenerMedioDePago():Observable <RespuestaGeneral>{
    return this.http.get<RespuestaGeneral>(`${this.URL}/resto/medio_de_pago/`)
  }
}
