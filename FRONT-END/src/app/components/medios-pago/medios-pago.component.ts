import { Component, OnInit } from '@angular/core';
import { MediosPagoService } from './medios-pago.service';

@Component({
  selector: 'app-medios-pago',
  templateUrl: './medios-pago.component.html',
  styleUrls: ['./medios-pago.component.css']
})
export class MediosPagoComponent implements OnInit {

  public mediodepagos : any = [];

  constructor(
    public MedioDePagoService : MediosPagoService,
  ) { }

  ngOnInit(): void {
    this.ListadoMediosdepago()
  }


  ListadoMediosdepago(){
    this.MedioDePagoService.ObtenerMedioDePago().subscribe(
      res => {
        console.log(res.datos)
        this.mediodepagos = res.datos
      },
      err => {
        console.log(err)
      }
    )
  }

}
