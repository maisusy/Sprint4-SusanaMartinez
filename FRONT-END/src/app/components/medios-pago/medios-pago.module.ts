import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MediosPagoComponent } from './medios-pago.component';
import { TableModule } from 'primeng/table';



@NgModule({
  declarations: [
    MediosPagoComponent
  ],
  exports: [
    MediosPagoComponent
  ],
  imports: [
    CommonModule,
    TableModule,
  ]
})
export class MediosPagoModule { }
