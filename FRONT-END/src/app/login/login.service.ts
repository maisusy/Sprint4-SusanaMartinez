import { HttpClient, HttpEvent, HttpHandler, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public url : string = environment.apiURL

  constructor(
    public http: HttpClient,
  ) { }

  Login(datos:any){
    return this.http.post<{msg:string,token:string}>(`${this.url}/resto/inicio/login`,datos)
  }

  CerrarSesion(){
    return this.http.put(`${this.url}/resto/inicio/CerrarSesion`,"")
  }

  GoogleLogin(){
    return this.http.get<{msg:string,token:string}>(`${this.url}/google/auth/callback`)
  }

}
