import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { LoginService } from './login.service';
import { InterceptorService } from '../interceptors/interceptor.service'
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [MessageService]
})
export class LoginComponent implements OnInit {

  public invalid : string = "";
  public url : string = environment.apiURL
  
  formsLogin = new FormGroup({
    'usuario': new FormControl('', Validators.required),
    'contrasenia': new FormControl('', Validators.required),
  })

  constructor(
    public loginService: LoginService,
    public router: Router,
    public InterceptorService : InterceptorService
  ) { }

  ngOnInit(): void {
  }

  InicioGoogle(){
    window.location.href = `${this.url}/google/auth`
  }

  InicioFacebook(){
    window.location.href = `${this.url}/facebook/auth`
  }

  InicioLinkedin(){
    window.location.href = `${this.url}/linkedin/auth`
  }
  

  enviar(){
    if(this.formsLogin.valid){
      this.loginService.Login(this.formsLogin.value).subscribe(
        res => {
          const token = res.token 
          console.log(res)
          this.router.navigate([`menu`], { queryParams: { token:token } });
        },
        err => {
          console.log(err)
        }
      )

    }else{
      this.invalid = "ng-dirty"
    }
  }

}
