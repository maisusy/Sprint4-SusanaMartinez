import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { LoginService } from '../login/login.service';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.css'],
})
export class MenuComponent implements OnInit {
    public items!: MenuItem[];
    public activeItem!: MenuItem;
    urlTree: any;

    constructor(public loginService: LoginService, public router: Router) {}

    ngOnInit() {
      //obtenenmos token y mandamos al header
        this.urlTree = this.router.parseUrl(this.router.url);
        localStorage.setItem('TOKEN', this.urlTree.queryParams['token']);

        this.items = [
            { label: 'Inicio', icon: 'pi pi-fw pi-home', command: () => {this.cambiarTab(0)} },
            { label: 'Productos', icon: 'pi pi-fw pi-home' , command: () => {this.cambiarTab(1)}},
            { label: 'Pedidos', icon: 'pi pi-fw pi-calendar', command: () => {this.cambiarTab(2)} },
            { label: 'Medios de Pago', icon: 'pi pi-fw pi-pencil', command: () => {this.cambiarTab(3)} },
            { label: 'Editar Perfil', icon: 'pi pi-fw pi-file', command: () => {this.cambiarTab(4)} },
        ];

        this.activeItem = this.items[1];
        
    }

    cambiarTab(posicion: number){
      this.activeItem.styleClass = '';
      this.activeItem = this.items[posicion];
      this.activeItem.styleClass = 'p-menuitem-link-active';
    }

    CerrarSesion() {
        this.loginService.CerrarSesion().subscribe(
            (res) => {
                console.log(res);
                localStorage.setItem('TOKEN', '');
                this.router.navigate(['login']);
            },
            (error) => {
                console.log(error);
            }
        );
    }
}
