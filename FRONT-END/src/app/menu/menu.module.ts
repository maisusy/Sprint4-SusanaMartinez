import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuRoutingModule } from './menu-routing.module';
import { MenuComponent } from './menu.component';
import { ProductosModule } from '../components/productos/productos.module';
import { ButtonModule } from 'primeng/button';import {MenubarModule} from 'primeng/menubar';
import { InputTextModule } from 'primeng/inputtext';
import {TabMenuModule} from 'primeng/tabmenu';
import { PedidosModule } from '../components/pedidos/pedidos.module';
import { MediosPagoModule } from '../components/medios-pago/medios-pago.module';
import { UsuarioModule } from '../components/usuario/usuario.module';

@NgModule({
  declarations: [
    MenuComponent
  ],
  imports: [
    CommonModule,
    MenuRoutingModule,
    ProductosModule,
    PedidosModule,
    MediosPagoModule,
    UsuarioModule,
    ButtonModule,
    MenubarModule,
    InputTextModule,
    TabMenuModule
  ]
})
export class MenuModule { }
