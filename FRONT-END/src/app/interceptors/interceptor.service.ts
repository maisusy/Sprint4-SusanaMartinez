import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class InterceptorService implements HttpInterceptor{

  constructor() { }
  
  public token!: any;

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(localStorage.getItem('TOKEN')==undefined){
       this.token = localStorage.setItem('TOKEN', '');
    }else{
       this.token = localStorage.getItem('TOKEN');
    }
    const headers = new HttpHeaders({
     'Content-Type': 'application/json',
     'Authorization': this.token,
     'Access-Control-Allow-Origin': '*',
     'Access-Control-Allow-Methods':'GET,POST,OPTIONS,DELETE,PUT'
    });
    const reqClone = req.clone({
      headers
    });

    return next.handle( reqClone);
  }
}
