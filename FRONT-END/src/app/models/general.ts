export interface RespuestaGeneral {
    msg: string;
    datos: any[];
  }

export interface RespuestaLogin {
  msg: string;
  token : string;
}