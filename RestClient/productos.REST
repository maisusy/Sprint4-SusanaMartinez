@HOST = http://localhost:8080/resto/productos

@TOKEN=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c3VhcmlvIjoiYWRtaW4iLCJub21fYXBlIjoiYWRtaW4iLCJjb3JyZW8iOiJhZG1pbkBnbWFpbC5jb20iLCJ0ZWxlZm9ubyI6MjY5ODQ1NTg1LCJkaXJlY2Npb24iOlsiY2FsbGUgMTIzIiwiZGlyZWNjaW9uIG51ZXZhIl0sImNvbnRyYXNlbmlhIjoiYWRtaW4iLCJhZG1pbiI6dHJ1ZSwibG9ndWVhZG8iOnRydWUsInN1c3BlbmRpZG8iOmZhbHNlLCJpYXQiOjE2NDM5MzUxNTMsImV4cCI6MTY0MzkzODc1M30.EIIHOU9aqDJVxYMGpajFkuJLxm_Uno6NVpA6NdrCuqY


###Crear producto
POST  {{HOST}}/alta HTTP/1.1
Content-Type: application/json
Authorization: {{TOKEN}} 

{
    "nombre":"rosquillas",
    "precio":100
}

###Listado de productos
GET  {{HOST}}/ HTTP/1.1
Content-Type: application/json
Authorization: {{TOKEN}} 
